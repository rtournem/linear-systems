% This text is proprietary.
% It's a part of presentation made by myself.
% It may not used commercial.
% The noncommercial use such as private and study is free
% Sep. 2005 
% Author: Sascha Frank 
% University Freiburg 
% www.informatik.uni-freiburg.de/~frank/


\documentclass{beamer}

\usepackage{hyperref}
\usepackage{amsmath}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[francais]{babel}


\defbeamertemplate{description item}{align left}{\insertdescriptionitem\hfill}
\DeclareMathOperator*{\argmin}{argmin} 

\begin{document}
\title{Advanced numerical methods }   
\author{Sébastien Bourguignon \& Robin Tournemenne} 
\date{\today} 

\frame{\titlepage} 

\frame{\frametitle{Table of contents}\tableofcontents} 


\section{Solving linear systems} 
	\frame{\frametitle{Linear system definition} 
		A system of $m$ linear equations involving a set of $n$ variables $x_1, x_2, ..., x_n$ is written:

		$$
			\left \{ \begin{matrix}
			a_{11}x_1 + a_{12}x_2 + ... + a_{1n}x_n & = & b_1 \\ 
			...&  & ... \\ 
			a_{m1}x_1 + a_{m2}x_2 + ... + a_{mn}x_n & = & b_m 
			\end{matrix}  \right.
		$$
		which is equivalent to the matrix equation $\mathbf{A}\mathbf{x}=\mathbf{b}$, with

		$$
			\mathbf{A}=\begin{bmatrix}
			a_{11} & \cdots & a_{1n}\\ 
			\vdots  &  \ddots &\vdots  \\ 
			a_{m1} & \cdots & a_{mn}
			\end{bmatrix} , \quad \mathbf{x}=\begin{bmatrix}
			x_{1}\\ 
			\vdots \\ 
			x_{n}
			\end{bmatrix} , \quad \mathbf{b}=\begin{bmatrix}
			b_{1}\\ 
			\vdots \\ 
			b_{m}
			\end{bmatrix} \neq 0
		$$

		$$
			\mathbf{A} \in \mathbb{R}^{m \times  n}, \quad \mathbf{x} \in  \mathbb{R}^{n}, \quad \mathbf{b} \in \mathbb{R}^{m} 
		$$

		\textbf{Is there any vector $\mathbf{x}$, called solution, verifying this equation?}
	}

	\subsection{Square Matrix}
		\frame{ \frametitle{Square Matrix: Solutions} 
			\begin{itemize}
				\item A is invertible ($det(\mathbf{A}) \neq 0$) : there is one solution, $\mathbf{x}=\mathbf{A}^{-1}\mathbf{b}$

				\vspace*{10pt}
				
				\item A is not invertible ($det(\mathbf{A})=0$) 
			
				\vspace*{10pt}
				
			\begin{itemize}
				\item $rank(\mathbf{A})=rank(\mathbf{Ab})$: infinity of solutions; the dimension of the solution's space is $m-rank(\mathbf{A})$  

				\vspace*{10pt}

				\item $rank(\mathbf{A}) \neq rank(\mathbf{Ab})$: there is no solution
			\end{itemize}
		\end{itemize}

}

		\frame[fragile=singleslide]{ \frametitle{Invertible Square Matrix}

			\begin{description}
				\item[The bad (and easy) way] 
				\begin{verbatim}
					x = inv(A)*b
				\end{verbatim}

				\vspace*{20pt}

				\item[The three "equivalent" functions] 
				\begin{verbatim}
					mldivide, linsolve, solve
				\end{verbatim}
			\end{description}
		}

		\frame[fragile=singleslide]{ \frametitle{Invertible Square Matrix: Example of direct inversion}

			\begin{verbatim}
				A=rand(3);
				b=rand(3,1);

				x=inv(A)*b;
		
				disp(A);
				disp(b);
				disp(x);	
			\end{verbatim}

			good in 90\% of the situations 
	
			{\color{red} inv(A) computes even if A non-invertible (numerical precision)}

			 Look for matlab internal orange warnings (matrix ill-conditioned) or/and compute 
			\begin{verbatim}
				A*inv(A)
			\end{verbatim}
			and check that you obtain $I$
	
		}

		\frame[fragile=singleslide]{\frametitle{Invertible Square Matrix: mldivide (1)}
			It does not compute the inverse of $\mathbf{A}$ (expensive), it is more robust and effective.
			
			There are two ways to call this function:
			\begin{verbatim}
				x=A\b;
				x=mldivide(A,B);
			\end{verbatim}
					\vspace*{-20pt}
			
			\begin{columns}
				\begin{column}{4cm}
					It also works with sparse Matrices:
				\end{column}
				\begin{column}{6cm}
					\begin{figure}
						\includegraphics[scale=0.24]{Pictures/sparse.png}
					\end{figure}				
				\end{column}
			\end{columns}
		}
		
		\frame{\frametitle{Invertible Square Matrix: mldivide (2)}
			very efficient function checking many properties of $\mathbf{A}$ before solving the system:
				
			\vspace*{-10pt}
		
			\begin{figure}
				\includegraphics[scale=0.47]{Pictures/mldivide_1.png}
			\end{figure}
		}
		
		\frame[fragile=singleslide]{\frametitle{Invertible Square Matrix: mldivide (3)}
			\begin{figure}
				\includegraphics[scale=0.30]{Pictures/mldivide_2.png}
			\end{figure}	
			\textbf{try mldivide() with the previous example,}
			\begin{verbatim} 
				x=A\b 
			\end{verbatim}
		}
		
		\frame[fragile=singleslide]{\frametitle{Invertible Square Matrix: linsolve (1)}
			A simpler version of mldivide regarding system resolution:
		%	\vspace*{20pt}
			\begin{table}[htdp]
				\begin{center}
					\begin{tabular}{|p{5cm}|p{5cm}|}
						\hline
						\textbf{mldivide:} & \textbf{linsolve:} \\ \hline
						mldivide utilizes the big decision tree presented on the previous slides which can run \textbf{7 different solvers} & X = linsolve(A,B) solves the linear system A*X = B using LU factorization with partial pivoting when A is square and QR factorization with column pivoting otherwise. \textbf{2 available solvers in a typical call}\\ \hline
		
					\end{tabular}
				\end{center}
			\end{table}%
		}
		
		\frame[fragile=singleslide]{\frametitle{Invertible Square Matrix: linsolve (2)}
			Yet, linsolve can deal with options regarding the shape and properties of $\mathbf{A}$:
			
			\begin{verbatim}
				A = triu(rand(5,3)); x = [1 1 1 0 0]'; b = A'*x;
				opts.UT = true; opts.TRANSA = true;
				y = linsolve(A,b,opts)
				disp(y);
			\end{verbatim}
			\vspace*{10pt}
			This usage accelerates processing time for large matrices
			
			\vspace*{10pt}
			
			BUT
			
			\vspace*{10pt}
			
			{\color{red} If opts has wrong properties then linsolve gives wrong results! VERY dangerous!!!}
			
			\vspace*{10pt}
			
			\textbf{try linsolve() with the previous example}
		}
		
		\frame[fragile=singleslide]{\frametitle{Invertible Square Matrix: solve, Introduction to symbolic variables}
			
			solve() is a far more general solver for any type of equation. 
			
			It does not deal with numbers like inv() or mldivide() but with \textbf{symbolic variables} like Maple or... \textbf{YOU}!
			
			\vspace*{10pt}
			
			IBM definition: 
			\begin{quote}
				A symbolic variable is a string of characters that you define as a symbol. 
				Because the variable is a symbol, you can assign different values to it at different times. 
				By assigning different values, you can do the same processing with different data.
			\end{quote}
				
			The symbolic variables being unconnected to numbers you can create equations:
			
			\begin{verbatim}
				syms x y z  %declare x y z as symbolic
				eqn = 2*x+3*y+7*z == 2 %eqn is also a symbolic variable 
			\end{verbatim}
		}
		
		\frame[fragile=singleslide]{\frametitle{Invertible Square Matrix: solve, example}
			\begin{verbatim}
				A=rand(3);
				b=rand(3,1);
				
				x=inv(A)*b;
				disp(x);
				
				syms x y z
				
				eq= A*[x; y; z]==b;
				
				pretty(eq) %prompt the symbolic variable
				
				Sol=solve(eq,[x,y,z]);
				pretty(Sol.x); %Sol.x is symbolic, no numerical values!
				disp(vpa(Sol.x)) %vpa converts sym. into num. values 
				disp(vpa(Sol.y))
				disp(vpa(Sol.z))
			\end{verbatim}

		}

		\frame[fragile=singleslide]{\frametitle{Non-Invertible Square Matrix}
			We have either \textbf{no solution} or an \textbf{infinity} of solutions.
			
			\vspace*{10pt}
			
			\underline{{\color{blue} The four methods:}}
			
			\vspace*{10pt}
		        
		        \setbeamertemplate{description item}[align left]
			\begin{description}
				\item[inv(A)*b] inv(A) will exist... {\color{red} NO}
				\item[mldivide(A,b)] no or one solution (out of infinity)  {\color{red} OK...}
				\item[linsolve(A,b)] no or one solution (lowest Norm) {\color{red} OK...}
				\item[solve(eqs,b,'ReturnConditions',true)] no or ALL solution {\color{green} PERFECT}
			\end{description}

		}
		
		\frame[fragile=singleslide]{\frametitle{Non-Invertible Square Matrix: Example}
			
			\begin{verbatim}
				A=[1,2,3;4,5,6;7,8,9];
				%case no solutions
				b=[1;4;2];
				%case infinity of solutions
				% b=[2;4;6];
				
				syms x y z
				eq= A*[x; y; z]==b;
				
				[xsol,ysol,zsol,param,cond]=solve(eq,[x,y,z],...
					'ReturnConditions',true);
				pretty(xsol);
				pretty(ysol);
				pretty(zsol);
			\end{verbatim}
		}
	
		\frame[fragile=singleslide]{\frametitle{Non-Invertible Square Matrix: Generalized Inverse, pinv()}
			
			Je ne pense pas avoir bien saisi l'interet de pinv...
		}
		
	\subsection{Non Square Matrix}
		\frame{\frametitle{Non Square Matrix}
			\begin{itemize}
				\item if $m > n$ (equations $>$ variables) and that $rank(A)>n$ overdetermined problem, {\color{red} no solutions}
				\item if $m < n$ (equations $<$ variables) underdetermined problem, {\color{red} infinity of solutions}
			\end{itemize}

			\vspace*{10pt}
			
			\underline{{\color{blue} The \textbf{three} methods:}}
			
			\vspace*{10pt}
	
			\setbeamertemplate{description item}[align left]
			\begin{description}
				\item[mldivide(A,b) and linsolve(A,b)] one solution using QR factorization  \\
				\begin{description}
					\item[overdetermined case:] least square best guess
					\item[undetermine case:] one of the infinity of solutions
				\end{description}
				\item[solve(eqs,b,'ReturnConditions',true)] no or ALL solutions {\color{green} PERFECT}
			\end{description}
		}

\section{Basic Curve fitting}
	\subsection{Linear Regression}
		\frame{\frametitle{Linear regression: the concept}
			
			\textbf{Two sets of data $U$ and $V$}
			
			What is the best linear relation between them?
			
			Mathematically: find $\alpha$ and $\beta$ such as
			
			$$
				\argmin_{\alpha,\beta}{J(x)}, \quad J(x) = \frac{1}{N} \sum_{i=1}^{N}{(v_i-(\alpha u_i+\beta))^{2}}
			$$
			
			More colloquially, what is the best $\alpha$, $\beta$ such as
			$$
				\alpha U +\beta \simeq V  \Leftrightarrow  \begin{bmatrix}
				u_1 & 1 \\ 
				\vdots & \vdots \\ 
				u_N & 1
				\end{bmatrix} \begin{bmatrix}
				\alpha \\ 
				\beta
				\end{bmatrix} \simeq V
				 \Leftrightarrow
				{\color{red} Ax \simeq b}
			$$
			with $ A=\begin{bmatrix}
				u_1 & 1 \\ 
				\vdots & \vdots \\ 
				u_N & 1
				\end{bmatrix}, \quad x=\begin{bmatrix}
				\alpha \\ 
				\beta
				\end{bmatrix}, \quad b=V $
		}
		\frame{\frametitle{Linear regression: Matlab stratey}
			
			We need to solve ${\color{red} Ax \simeq b}$ in the \textbf{least square sense}

			\vspace*{10pt}
			
			overdetermined case m$>$n (set size$>$model parameters)

			\vspace*{10pt}
			
			This is exactly the solution \textbf{mldivide(A,b)} provides!			
		}

		\frame[fragile=singleslide]{\frametitle{Linear regression: Example}
			
			\begin{verbatim}
				U=(1:30)'; %first dataset
				V=U.*3+15.*rand(30,1); %second dataset
				
				figure; plot(U,V,'o');hold on;
				
				AlBet=mldivide([U ones(30,1)],V);
				
				plot(U,AlBet(1).*U+AlBet(2),'r');

			\end{verbatim}
			\begin{figure}
				\includegraphics[scale=0.25]{Pictures/regression.png}
			\end{figure}	
		}
		
		
	\subsection{Interpolation}
		\frame{\frametitle{Interpolation: interp1}
			Interpolation is used to guess values of a \textbf{discretized curve} over $\mathbb{R}$.
			
			interp1 possesses different strategies to \textbf{link the points} of the discretized curve:
				
			\vspace*{-20pt}
			
			\begin{columns}
				\begin{column}{5cm}
					\begin{figure}
						\includegraphics[scale=0.44]{Pictures/interp1_1.png}
					\end{figure}				\end{column}
				\begin{column}{5cm}
					\begin{figure}
						\includegraphics[scale=0.44]{Pictures/interp1_2.pdf}
					\end{figure}				
				\end{column}
			\end{columns}	
			 
		}	
		\frame[fragile=singleslide]{\frametitle{Interpolation: Example}
			\begin{quote}
				Taken from Matlab Help
			\end{quote}			
			\begin{verbatim}
				x = 0:pi/4:2*pi; %discrete abscissae
				v = sin(x); %discrete ordinates
				
				xq = 0:pi/16:2*pi; %interpolation abscissae
				figure
				vq1 = interp1(x,v,xq); %interpolation values
				plot(x,v,'o',xq,vq1,':.');
				xlim([0 2*pi]);
				title('(Default) Linear Interpolation');				
			\end{verbatim}

			 
		}	
		
\section{Fourier analysis}
		\frame{\frametitle{Fourier analysis: basic knowledge}
				
			\begin{itemize}
				\item Spectral analysis everywhere under many forms
				\item 99.99 \% of the time, \textbf{real discrete finite signal} with a \textbf{sampling frequency $F_s$} $\Rightarrow$ DFT (Discrete Fourier Transform) performed by the FFT algorithm (Fast Fourier Transform) 
				\item Under these conditions, only half on the DFT points give us information:
				\begin{itemize}
					\item $F_{max} = \frac{F_s}{2}$ 
					\item $\Delta F= \frac{F_s}{N}$
				\end{itemize}
			\end{itemize}
		}
		\frame[fragile=singleslide]{\frametitle{Fourier analysis: Matlab Example}
			\begin{quote}
				Taken from Matlab Help
			\end{quote}
			\begin{verbatim}
				Fs = 1000;            % Sampling frequency
				T = 1/Fs;             % Sampling period
				L = 1000;             % Length of signal
				t = (0:L-1)*T;        % Time vector
				S = 0.7*sin(2*pi*50*t) + sin(2*pi*120*t);
				figure;
				plot(1000*t(1:50),X(1:50));
				
				Y = fft(X);
				P2 = abs(Y/L);
				P1 = P2(1:L/2+1);
				P1(2:end-1) = 2*P1(2:end-1);
				f = Fs*(0:(L/2))/L;
				plot(f,P1)

			\end{verbatim}
		}
\end{document}

